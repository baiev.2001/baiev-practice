# Practice work
This project was created by Victor Baiev, a student 
of Black Sea National University, as technological practice.

**Theme of practice**: 
*Automation of business processes based on BPMN2 and Camunda*

## How to run

To run this demo application, you should go to devops folder. 
There you can find run.sh script. 

You should specify `MAIL_USER` and `MAIL_PASSWORD` variables to send email successfully.
Default configuration works with gmail user. If you will use some other email service
You will need change all of the `MAIL` variables, specified in `run.sh` script.

To start application run the command `./run.sh start`. 
After that you can go to `localhost:4200` path, and will 
see the front-end part of test application

## Description
This project is devided to front-end and back-end part. 
Back-end is written with *Java* and *Spring framework*. 
Front-end was created using *Angular* and *Type-Script* language.
### Back-end
Back-end is devided to *camunda* and *api* parts. *Camunda* module contains 
camunda-engine, which starts instance of Camunda BPM.<br>
To deploy a new process definition, you can simply put 
.bpmn file in a resource folder of this module, or use REST method 
[POST deployment](https://docs.camunda.org/manual/7.8/reference/rest/deployment/post-deployment/).
In the project there are already defined one process 
definition for user registration.
![registration process](https://i.piccy.info/i9/80b0c22d4f46d5af39dde55868464dcd/1585813990/27467/1370682/photo_2020_04_01_22_35_37.jpg)

#### Api

Api module is divided into several modules:
<ul>
    <li>Core</li>
    <li>Security</li>
    <li>User-services</li>
    <li>Camunda-integration</li>
</ul>       
Core module contains main entities and business logic of project. 
There you can find database entities and services for work with them.

Security module contains classes, needed for security configuration and service 
for user auth.

Camunda-integration module contains logic to work with camunda. It executes 
external task client and allows to create a subscription for external task. 
Subscribers listens to task based on a topic of task.

User-services module contains controllers and services for user registration. 
Also you can find there external task subscribers, which listens to Camunda BPM tasks.
As soon as process instance will stop at specific external task, subscriber will fetch it and process.
In case if any exception is thrown, task won't be completed, and process will stuck on this state.

### Front-end
Front-end module is divided on two modules:
<ul>
    <li>Login</li>
    <li>Registration</li>
</ul>
Login module contains login component, where you can authorize and go to your home page.

Registration module contains several components for registration of user. 

Also there are core package, where you can find services, models, 
auth interceptor and guards, that protects specific paths from unauthorized users.

## Others

### Credentials
|   Source      | Login         | Password  |
| ------------- |:-------------:|:---------:|
| Database      | sa            |           |
| Camunda       | pass          | pass      |
| Application   | test          | test      |

### Ports
<ul>
 <li>Camunda - 8080</li>
 <li>User-service application - 8090</li>
 <li>Angular front-end - 4200</li>
</ul>
