package edu.chmnu.baiev.practice.integration.camunda;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.ExternalTaskClient;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Slf4j
public class CamundaExternalTaskClient {
    private final ExternalTaskClient externalTaskClient;

    public CamundaExternalTaskClient(ExternalTaskClient externalTaskClient) {
        this.externalTaskClient = externalTaskClient;
    }

    @PostConstruct
    public void init() {
        log.debug("Starting external task client");
        externalTaskClient.start();
    }

    public void subscribe(ExternalTaskSubscription subscription) {
        log.debug("Subscribing for topic: " + subscription.getTopic());
        externalTaskClient.subscribe(subscription.getTopic())
            .lockDuration(100000)
            .handler(subscription.getExternalTaskHandler())
            .open();
    }

    @PreDestroy
    void kill() {
        log.debug("Stopping external task client");
        externalTaskClient.stop();
    }
}
