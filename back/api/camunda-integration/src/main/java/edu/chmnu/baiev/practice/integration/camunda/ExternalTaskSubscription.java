package edu.chmnu.baiev.practice.integration.camunda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.camunda.bpm.client.task.ExternalTaskHandler;

@Data
@Builder
@AllArgsConstructor
public class ExternalTaskSubscription {
    private String topic;

    private ExternalTaskHandler externalTaskHandler;
}
