package edu.chmnu.baiev.practice.integration.camunda.conf;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {
    "edu.chmnu.baiev.practice.integration.camunda"
})
@EnableFeignClients(basePackages = {
    "edu.chmnu.baiev.practice.integration.camunda.controllers"
})
public class CamundaIntegrationConfiguration {
}
