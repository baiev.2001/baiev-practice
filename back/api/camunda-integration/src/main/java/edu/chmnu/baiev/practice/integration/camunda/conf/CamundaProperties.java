package edu.chmnu.baiev.practice.integration.camunda.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "camunda")
public class CamundaProperties {

    private String url;

    private Integer maxTasks;

    private String workerId;
}
