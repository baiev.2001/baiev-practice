package edu.chmnu.baiev.practice.integration.camunda.conf;

import org.camunda.bpm.client.ExternalTaskClient;
import org.camunda.bpm.client.ExternalTaskClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Optional.ofNullable;

@Configuration
public class ExternalTaskClientConfiguration {
    @Bean
    public ExternalTaskClient externalTaskClient(CamundaProperties properties) {
        ExternalTaskClientBuilder externalTaskClientBuilder = ExternalTaskClient.create();
        externalTaskClientBuilder.baseUrl(properties.getUrl());
        externalTaskClientBuilder.maxTasks(ofNullable(properties.getMaxTasks()).orElse(5));
        ofNullable(properties.getWorkerId()).ifPresent(externalTaskClientBuilder::workerId);
        return externalTaskClientBuilder.build();
    }
}
