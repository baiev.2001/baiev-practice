package edu.chmnu.baiev.practice.integration.camunda.controllers;

import org.camunda.bpm.engine.rest.dto.runtime.ExecutionTriggerDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "execution", url = "${camunda.url}")
@RequestMapping("/execution")
public interface ExecutionController {

    @PostMapping("/{executionId}/signal")
    void signal(@PathVariable("executionId") String executionId,
                @RequestBody ExecutionTriggerDto triggerDto);
}
