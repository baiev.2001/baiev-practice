package edu.chmnu.baiev.practice.integration.camunda.controllers;

import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceQueryDto;
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "process", url = "${camunda.url}")
@RequestMapping
public interface ProcessController {

    @PostMapping("/process-definition/key/{key}/start")
    ProcessInstanceDto startProcessByKey(@PathVariable("key") String processDefinitionKey,
                                         @RequestBody StartProcessInstanceDto dto);

    @PostMapping("/process-instance")
    ProcessInstanceDto[] getInstance(@RequestParam("firstResult") Integer firstResult,
                                     @RequestParam("maxResults") Integer maxResults,
                                     @RequestBody ProcessInstanceQueryDto processInstanceQueryDto);
}
