package edu.chmnu.baiev.practice.integration.camunda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class ProcessDto {
    private String businessKey;

    private String id;

    private String definitionId;
}
