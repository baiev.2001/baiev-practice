package edu.chmnu.baiev.practice.integration.camunda.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.camunda.bpm.engine.variable.VariableMap;

import java.util.Map;

@Data
@Builder
@AllArgsConstructor
public class StartProcessDto {
    protected Map<String, Object> variables;

    protected String businessKey;

    protected String caseInstanceId;

    protected boolean skipCustomListeners;

    protected boolean skipIoMappings;

    protected boolean withVariablesInReturn;
}
