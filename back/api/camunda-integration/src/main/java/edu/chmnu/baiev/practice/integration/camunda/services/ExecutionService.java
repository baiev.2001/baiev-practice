package edu.chmnu.baiev.practice.integration.camunda.services;

import java.util.Map;

public interface ExecutionService {

    void signal(String executionId);
}
