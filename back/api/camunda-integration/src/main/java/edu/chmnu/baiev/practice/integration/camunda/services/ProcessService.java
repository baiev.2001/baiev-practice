package edu.chmnu.baiev.practice.integration.camunda.services;

import edu.chmnu.baiev.practice.integration.camunda.dto.ProcessDto;
import edu.chmnu.baiev.practice.integration.camunda.dto.StartProcessDto;

public interface ProcessService {

    ProcessDto startProcessByKey(String processDefinitionKey, StartProcessDto dto);

    ProcessDto findByBusinessKey(String businessKey);
}
