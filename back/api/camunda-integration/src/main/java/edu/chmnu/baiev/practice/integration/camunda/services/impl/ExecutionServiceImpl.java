package edu.chmnu.baiev.practice.integration.camunda.services.impl;

import edu.chmnu.baiev.practice.integration.camunda.controllers.ExecutionController;
import edu.chmnu.baiev.practice.integration.camunda.services.ExecutionService;
import org.camunda.bpm.engine.rest.dto.runtime.ExecutionTriggerDto;
import org.springframework.stereotype.Service;

@Service
public class ExecutionServiceImpl implements ExecutionService {

    private final ExecutionController executionController;

    public ExecutionServiceImpl(ExecutionController executionController) {
        this.executionController = executionController;
    }

    @Override
    public void signal(String executionId) {
        ExecutionTriggerDto executionTriggerDto = new ExecutionTriggerDto();
        executionController.signal(executionId, executionTriggerDto);
    }
}
