package edu.chmnu.baiev.practice.integration.camunda.services.impl;

import edu.chmnu.baiev.practice.integration.camunda.controllers.ProcessController;
import edu.chmnu.baiev.practice.integration.camunda.dto.ProcessDto;
import edu.chmnu.baiev.practice.integration.camunda.dto.StartProcessDto;
import edu.chmnu.baiev.practice.integration.camunda.services.ProcessService;
import org.camunda.bpm.engine.rest.dto.VariableValueDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceDto;
import org.camunda.bpm.engine.rest.dto.runtime.ProcessInstanceQueryDto;
import org.camunda.bpm.engine.rest.dto.runtime.StartProcessInstanceDto;
import org.camunda.bpm.engine.variable.VariableMap;
import org.camunda.bpm.engine.variable.impl.VariableMapImpl;
import org.springframework.stereotype.Service;

import java.util.Map;

import static java.util.Optional.ofNullable;

@Service
public class ProcessServiceImpl implements ProcessService {
    private final ProcessController processController;

    public ProcessServiceImpl(ProcessController processController) {
        this.processController = processController;
    }

    @Override
    public ProcessDto startProcessByKey(String processDefinitionKey, StartProcessDto startProcessDto) {
        StartProcessInstanceDto startProcessInstanceDto = new StartProcessInstanceDto();
        startProcessInstanceDto.setBusinessKey(startProcessDto.getBusinessKey());
        ofNullable(startProcessDto.getVariables())
            .ifPresent(variableMap -> startProcessInstanceDto.setVariables(VariableValueDto.fromMap(this.toVariableMap(variableMap))));
        ProcessInstanceDto processInstanceDto = processController.startProcessByKey(processDefinitionKey, startProcessInstanceDto);
        return ProcessDto.builder()
            .id(processInstanceDto.getId())
            .businessKey(processInstanceDto.getBusinessKey())
            .definitionId(processInstanceDto.getDefinitionId())
            .build();
    }

    @Override
    public ProcessDto findByBusinessKey(String businessKey) {
        ProcessInstanceQueryDto processInstanceQueryDto = new ProcessInstanceQueryDto();
        processInstanceQueryDto.setBusinessKey(businessKey);
        ProcessInstanceDto instance = processController.getInstance(0, 1, processInstanceQueryDto)[0];
        return ProcessDto.builder()
            .id(instance.getId())
            .businessKey(instance.getBusinessKey())
            .definitionId(instance.getDefinitionId())
            .build();
    }

    private VariableMap toVariableMap(Map<String, Object> map) {
        return new VariableMapImpl(map);
    }
}
