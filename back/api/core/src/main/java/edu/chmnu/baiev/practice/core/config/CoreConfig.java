package edu.chmnu.baiev.practice.core.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@ComponentScan(basePackages = {
    "edu.chmnu.baiev.practice.core"
})
public class CoreConfig {

}
