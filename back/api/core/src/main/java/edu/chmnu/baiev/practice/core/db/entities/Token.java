package edu.chmnu.baiev.practice.core.db.entities;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tokens")
public class Token {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "code")
    private String code;
}
