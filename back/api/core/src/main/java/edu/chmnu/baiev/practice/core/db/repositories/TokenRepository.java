package edu.chmnu.baiev.practice.core.db.repositories;

import edu.chmnu.baiev.practice.core.db.entities.Token;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends CrudRepository<Token, Long> {

    @Query("select t from Token as t where t.code=:code")
    Optional<Token> findByCode(@Param("code") String code);
}
