package edu.chmnu.baiev.practice.core.db.repositories;

import edu.chmnu.baiev.practice.core.db.entities.User;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("SELECT u from User u WHERE u.login=:login")
    Optional<User> findByLogin(@Param("login") String login);

    @Modifying
    @Query("update User u set u.enabled = :enabled where u.id = :id")
    int updateEnabled(@Param("id") Long id, @Param("enabled") boolean enabled);
}
