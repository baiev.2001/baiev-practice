package edu.chmnu.baiev.practice.core.mappers;

public interface GenericMapper <E, D> {

    E toEntity(D dto);

    D toDto(E entity);
}
