package edu.chmnu.baiev.practice.core.mappers;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.dto.UserDto;
import org.springframework.stereotype.Component;

@Component
public class UserUserDtoMapper implements GenericMapper<User, UserDto> {

    @Override
    public User toEntity(UserDto dto) {
        return User.builder()
            .email(dto.getEmail())
            .login(dto.getLogin())
            .build();
    }

    @Override
    public UserDto toDto(User entity) {
        return UserDto.builder()
            .login(entity.getLogin())
            .email(entity.getEmail())
            .build();
    }
}
