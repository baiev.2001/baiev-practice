package edu.chmnu.baiev.practice.core.services;

import edu.chmnu.baiev.practice.core.db.entities.Token;

public interface TokenService {
    Token create(Token token);

    Token getById(Long tokenId);

    Token getByCode(String code);
}
