package edu.chmnu.baiev.practice.core.services;

import edu.chmnu.baiev.practice.core.db.entities.User;

public interface UserService {

    User create(User user);

    User getById(Long id);

    User getByLogin(String login);

    void setActive(Long id);
}
