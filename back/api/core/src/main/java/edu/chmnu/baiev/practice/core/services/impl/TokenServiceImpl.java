package edu.chmnu.baiev.practice.core.services.impl;

import edu.chmnu.baiev.practice.core.db.entities.Token;
import edu.chmnu.baiev.practice.core.db.repositories.TokenRepository;
import edu.chmnu.baiev.practice.core.exceptions.TokenNotFoundException;
import edu.chmnu.baiev.practice.core.services.TokenService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TokenServiceImpl implements TokenService {
    private final TokenRepository tokenRepository;

    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    @Transactional
    public Token create(Token token) {
        return tokenRepository.save(token);
    }

    @Override
    @Transactional(readOnly = true)
    public Token getById(Long tokenId) {
        return tokenRepository.findById(tokenId).orElseThrow(TokenNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public Token getByCode(String code) {
        return tokenRepository.findByCode(code).orElseThrow(TokenNotFoundException::new);
    }
}
