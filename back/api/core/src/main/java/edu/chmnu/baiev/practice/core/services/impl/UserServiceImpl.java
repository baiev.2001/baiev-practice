package edu.chmnu.baiev.practice.core.services.impl;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.db.repositories.UserRepository;
import edu.chmnu.baiev.practice.core.exceptions.UserNotFoundException;
import edu.chmnu.baiev.practice.core.services.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional
    public User create(User user) {
        return this.userRepository.save(user);
    }

    @Override
    @Transactional(readOnly = true)
    public User getById(Long id) {
        return this.userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional(readOnly = true)
    public User getByLogin(String login) {
        return this.userRepository.findByLogin(login).orElseThrow(UserNotFoundException::new);
    }

    @Override
    @Transactional
    public void setActive(Long id) {
        this.userRepository.updateEnabled(id, true);
    }
}
