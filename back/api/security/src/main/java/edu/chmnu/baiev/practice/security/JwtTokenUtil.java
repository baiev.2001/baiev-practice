package edu.chmnu.baiev.practice.security;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.security.dto.JwtTokenDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import static edu.chmnu.baiev.practice.core.utils.DateToLocalDateTimeConverter.toLocalDateTime;

@Component
public class JwtTokenUtil {

    @Value("${jwt.secret:#{'Q=d.9$'}}")
    private String secret;

    public String getLoginFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    public JwtTokenDto generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        Date issuedAt = new Date(System.currentTimeMillis());
        Date expireDate = new Date(issuedAt.getTime() + 3600000); //one hour
        String token = doGenerateToken(claims, user.getLogin(), issuedAt, expireDate);
        return JwtTokenDto.builder()
            .token(token)
            .created(toLocalDateTime(issuedAt))
            .expired(toLocalDateTime(expireDate))
            .build();
    }

    private String doGenerateToken(Map<String, Object> claims, String subject, Date issuedAt, Date expireDate) {
        return Jwts.builder()
            .setClaims(claims)
            .setSubject(subject)
            .setIssuedAt(issuedAt)
            .setExpiration(expireDate)
            .signWith(SignatureAlgorithm.HS512, secret)
            .compact();
    }

    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getLoginFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }


    private Claims getAllClaimsFromToken(String token) {
        //fixme can throw io.jsonwebtoken.MalformedJwtException: Unable to read JSON value
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

}
