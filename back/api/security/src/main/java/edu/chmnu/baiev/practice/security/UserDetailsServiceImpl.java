package edu.chmnu.baiev.practice.security;

import edu.chmnu.baiev.practice.core.db.repositories.UserRepository;
import edu.chmnu.baiev.practice.core.exceptions.UserNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new SecurityUserDetails(userRepository.findByLogin(username).orElseThrow(UserNotFoundException::new));
    }
}
