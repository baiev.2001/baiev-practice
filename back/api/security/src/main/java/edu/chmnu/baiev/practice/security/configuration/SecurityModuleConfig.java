package edu.chmnu.baiev.practice.security.configuration;

import edu.chmnu.baiev.practice.core.config.CoreConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(CoreConfig.class)
@ComponentScan(basePackages = "edu.chmnu.baiev.practice.security")
class SecurityModuleConfig {
}
