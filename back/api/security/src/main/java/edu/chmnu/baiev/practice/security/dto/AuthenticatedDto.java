package edu.chmnu.baiev.practice.security.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthenticatedDto {
    private String token;

    private LocalDateTime expireDate;
}
