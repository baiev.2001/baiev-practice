package edu.chmnu.baiev.practice.security.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JwtTokenDto {
    private String token;

    private LocalDateTime created;

    private LocalDateTime expired;
}
