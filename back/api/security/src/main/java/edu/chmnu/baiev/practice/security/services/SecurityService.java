package edu.chmnu.baiev.practice.security.services;

import edu.chmnu.baiev.practice.core.dto.UserDto;
import edu.chmnu.baiev.practice.security.dto.AuthenticatedDto;

public interface SecurityService {

    AuthenticatedDto login(String login, String password);

    UserDto getInfo(String token);
}
