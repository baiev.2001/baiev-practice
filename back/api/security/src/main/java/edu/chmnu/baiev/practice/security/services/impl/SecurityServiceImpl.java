package edu.chmnu.baiev.practice.security.services.impl;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.dto.UserDto;
import edu.chmnu.baiev.practice.core.mappers.UserUserDtoMapper;
import edu.chmnu.baiev.practice.core.services.UserService;
import edu.chmnu.baiev.practice.security.JwtTokenUtil;
import edu.chmnu.baiev.practice.security.dto.AuthenticatedDto;
import edu.chmnu.baiev.practice.security.dto.JwtTokenDto;
import edu.chmnu.baiev.practice.security.services.SecurityService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityServiceImpl implements SecurityService {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserUserDtoMapper userDtoMapper;

    public SecurityServiceImpl(UserService userService,
                               AuthenticationManager authenticationManager,
                               JwtTokenUtil jwtTokenUtil,
                               UserUserDtoMapper userDtoMapper) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDtoMapper = userDtoMapper;
    }

    @Override
    public AuthenticatedDto login(String login, String password) {
        User user = userService.getByLogin(login);
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(login, password);
        Authentication authenticate = authenticationManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        JwtTokenDto jwtTokenDto = jwtTokenUtil.generateToken(user);
        return AuthenticatedDto.builder()
            .token(jwtTokenDto.getToken())
            .expireDate(jwtTokenDto.getExpired())
            .build();
    }

    @Override
    public UserDto getInfo(String token) {
        String login = jwtTokenUtil.getLoginFromToken(token);
        User user = userService.getByLogin(login);
        return userDtoMapper.toDto(user);
    }
}
