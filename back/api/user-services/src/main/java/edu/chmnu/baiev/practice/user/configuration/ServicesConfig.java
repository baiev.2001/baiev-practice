package edu.chmnu.baiev.practice.user.configuration;

import edu.chmnu.baiev.practice.core.config.CoreConfig;
import edu.chmnu.baiev.practice.integration.camunda.conf.CamundaIntegrationConfiguration;
import edu.chmnu.baiev.practice.security.configuration.CommonSecurityProviders;
import org.h2.server.web.WebServlet;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@Import({
    CoreConfig.class,
    CommonSecurityProviders.class,
    CamundaIntegrationConfiguration.class
})
@EnableJpaRepositories(basePackages = {
    "edu.chmnu.baiev.practice.core.db.repositories"
})
@EntityScan(basePackages = {
    "edu.chmnu.baiev.practice.core.db.entities"
})
public class ServicesConfig {

    @Bean
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        return registrationBean;
    }
}
