package edu.chmnu.baiev.practice.user.controllers;

import edu.chmnu.baiev.practice.core.dto.UserDto;
import edu.chmnu.baiev.practice.security.dto.AuthenticatedDto;
import edu.chmnu.baiev.practice.security.dto.LoginDto;
import edu.chmnu.baiev.practice.security.services.SecurityService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    private final SecurityService securityService;

    public UserController(SecurityService securityService) {
        this.securityService = securityService;
    }

    @PostMapping(value = "/auth", produces = MediaType.APPLICATION_JSON_VALUE)
    public AuthenticatedDto authenticate(@RequestBody LoginDto loginDto) {
        return securityService.login(loginDto.getLogin(), loginDto.getPassword());
    }

    @GetMapping(value = "/info", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUser(@RequestHeader("X-Auth-Token") String token) {
        return securityService.getInfo(token);
    }
}
