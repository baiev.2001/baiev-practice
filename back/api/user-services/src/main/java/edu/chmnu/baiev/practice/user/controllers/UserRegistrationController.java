package edu.chmnu.baiev.practice.user.controllers;

import edu.chmnu.baiev.practice.user.dto.RegisterUserDto;
import edu.chmnu.baiev.practice.user.dto.RegisteredUserDto;
import edu.chmnu.baiev.practice.user.facades.UserRegistrationFacade;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user/register")
public class UserRegistrationController {
    private final UserRegistrationFacade userFacade;

    public UserRegistrationController(UserRegistrationFacade userFacade) {
        this.userFacade = userFacade;
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RegisteredUserDto register(@RequestBody RegisterUserDto userDto) {
        return userFacade.register(userDto);
    }

    @PostMapping(value = "/confirm")
    public ResponseEntity confirmUser(@RequestBody String tokenCode) {
        return userFacade.confirm(tokenCode)
            ? ResponseEntity.ok().build() :
            ResponseEntity.status(401).body("Unauthorized");
    }
}
