package edu.chmnu.baiev.practice.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisteredUserDto {

    private Long id;

    private String login;

    private String mail;
}
