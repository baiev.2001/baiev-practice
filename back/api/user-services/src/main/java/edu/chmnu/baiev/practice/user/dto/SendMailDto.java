package edu.chmnu.baiev.practice.user.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.InputStream;

@Data
@Builder
@AllArgsConstructor
public class SendMailDto {
    private String from;

    private String to;

    private String subject;

    private InputStream content;
}
