package edu.chmnu.baiev.practice.user.facades;

import edu.chmnu.baiev.practice.user.dto.RegisterUserDto;
import edu.chmnu.baiev.practice.user.dto.RegisteredUserDto;

public interface UserRegistrationFacade {

    RegisteredUserDto register(RegisterUserDto userDto);

    boolean confirm(String tokenCode);
}
