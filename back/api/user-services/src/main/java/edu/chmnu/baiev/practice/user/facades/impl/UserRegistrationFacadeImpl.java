package edu.chmnu.baiev.practice.user.facades.impl;

import edu.chmnu.baiev.practice.core.db.entities.Token;
import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.exceptions.TokenNotFoundException;
import edu.chmnu.baiev.practice.core.services.TokenService;
import edu.chmnu.baiev.practice.core.services.UserService;
import edu.chmnu.baiev.practice.integration.camunda.dto.ProcessDto;
import edu.chmnu.baiev.practice.integration.camunda.dto.StartProcessDto;
import edu.chmnu.baiev.practice.integration.camunda.services.ExecutionService;
import edu.chmnu.baiev.practice.integration.camunda.services.ProcessService;
import edu.chmnu.baiev.practice.user.dto.RegisterUserDto;
import edu.chmnu.baiev.practice.user.dto.RegisteredUserDto;
import edu.chmnu.baiev.practice.user.facades.UserRegistrationFacade;
import edu.chmnu.baiev.practice.user.mappers.UserRegisterUserDtoMapper;
import edu.chmnu.baiev.practice.user.mappers.UserRegisteredUserDtoMapper;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class UserRegistrationFacadeImpl implements UserRegistrationFacade {

    private final UserService userService;

    private final ProcessService processService;

    private final ExecutionService executionService;

    private final UserRegisterUserDtoMapper registerUserDtoMapper;

    private final UserRegisteredUserDtoMapper registeredUserDtoMapper;

    private final TokenService tokenService;

    public UserRegistrationFacadeImpl(UserService userService,
                                      ProcessService processService,
                                      ExecutionService executionService,
                                      UserRegisterUserDtoMapper registerUserDtoMapper,
                                      UserRegisteredUserDtoMapper registeredUserDtoMapper,
                                      TokenService tokenService) {
        this.userService = userService;
        this.processService = processService;
        this.executionService = executionService;
        this.registerUserDtoMapper = registerUserDtoMapper;
        this.registeredUserDtoMapper = registeredUserDtoMapper;
        this.tokenService = tokenService;
    }

    @Override
    @Transactional
    public RegisteredUserDto register(RegisterUserDto registerUserDto) {
        User user = registerUserDtoMapper.toEntity(registerUserDto);
        User registeredUser = userService.create(user);
        Token token = this.createToken(user);
        processService.startProcessByKey("UserRegistrationProcess", StartProcessDto.builder()
            .businessKey(registeredUser.getId().toString())
            .variables(Collections.singletonMap("tokenId", token.getId()))
            .build());
        return registeredUserDtoMapper.toDto(user);
    }

    private Token createToken(User user) {
        Token token = new Token();
        token.setUser(user);
        token.setCode(RandomStringUtils.randomAlphanumeric(8));
        return tokenService.create(token);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean confirm(String tokenCode) {
        try {
            Token token = tokenService.getByCode(tokenCode);
            ProcessDto processDto = processService.findByBusinessKey(token.getUser().getId().toString());
            executionService.signal(processDto.getId());
            return true;
        } catch (TokenNotFoundException ex) {
            return false;
        }
    }
}
