package edu.chmnu.baiev.practice.user.mappers;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.mappers.GenericMapper;
import edu.chmnu.baiev.practice.user.dto.RegisterUserDto;
import org.springframework.stereotype.Component;

@Component
public class UserRegisterUserDtoMapper implements GenericMapper<User, RegisterUserDto> {
    @Override
    public User toEntity(RegisterUserDto dto) {
        return User.builder()
            .login(dto.getLogin())
            .email(dto.getEmail())
            .password(dto.getPassword())
            .build();
    }

    @Override
    public RegisterUserDto toDto(User entity) {
        throw new UnsupportedOperationException();
    }
}
