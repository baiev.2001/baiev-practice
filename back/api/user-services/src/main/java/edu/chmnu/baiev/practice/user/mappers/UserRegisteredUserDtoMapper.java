package edu.chmnu.baiev.practice.user.mappers;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.mappers.GenericMapper;
import edu.chmnu.baiev.practice.user.dto.RegisteredUserDto;
import org.springframework.stereotype.Component;

@Component
public class UserRegisteredUserDtoMapper implements GenericMapper<User, RegisteredUserDto> {
    @Override
    public User toEntity(RegisteredUserDto dto) {
        throw new UnsupportedOperationException();
    }

    @Override
    public RegisteredUserDto toDto(User entity) {
        return RegisteredUserDto.builder()
            .id(entity.getId())
            .login(entity.getLogin())
            .mail(entity.getEmail())
            .build();
    }
}
