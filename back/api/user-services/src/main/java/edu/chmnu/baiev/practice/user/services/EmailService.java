package edu.chmnu.baiev.practice.user.services;

import edu.chmnu.baiev.practice.user.dto.SendMailDto;

public interface EmailService {
    void sendMail(SendMailDto sendMailDto);
}
