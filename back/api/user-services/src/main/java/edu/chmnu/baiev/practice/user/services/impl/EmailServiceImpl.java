package edu.chmnu.baiev.practice.user.services.impl;

import edu.chmnu.baiev.practice.user.dto.SendMailDto;
import edu.chmnu.baiev.practice.user.services.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.IOException;

@Component
@Slf4j
public class EmailServiceImpl implements EmailService {

    private final JavaMailSender javaMailSender;

    public EmailServiceImpl(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Override
    public void sendMail(SendMailDto dto) {
        try {
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();

            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(dto.getTo());
            messageHelper.setFrom(dto.getFrom());
            messageHelper.setSubject(dto.getSubject());

            MimeMultipart multipart = new MimeMultipart();
            MimeBodyPart bodyPart = new MimeBodyPart();
            bodyPart.setText(IOUtils.toString(dto.getContent()));
            multipart.addBodyPart(bodyPart);
            mimeMessage.setContent(multipart);

            javaMailSender.send(mimeMessage);
        } catch (MessagingException ex) {
            log.debug("Problems while creating mail template");
            throw new RuntimeException(ex);
        } catch (IOException ex) {
            log.debug("Problems while processing content");
            throw new RuntimeException(ex);
        }
    }
}
