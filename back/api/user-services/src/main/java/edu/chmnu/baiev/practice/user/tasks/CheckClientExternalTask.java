package edu.chmnu.baiev.practice.user.tasks;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.services.UserService;
import edu.chmnu.baiev.practice.integration.camunda.CamundaExternalTaskClient;
import edu.chmnu.baiev.practice.integration.camunda.ExternalTaskSubscription;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Collections;

@Component
@Slf4j
public class CheckClientExternalTask {

    private final UserService userService;
    private final CamundaExternalTaskClient externalTaskClient;

    public CheckClientExternalTask(UserService userService,
                                   CamundaExternalTaskClient externalTaskClient) {
        this.userService = userService;
        this.externalTaskClient = externalTaskClient;
    }

    @PostConstruct
    public void subscribe() {
        ExternalTaskSubscription build = ExternalTaskSubscription.builder()
            .topic("dataValidation")
            .externalTaskHandler((ExternalTask externalTask, ExternalTaskService externalTaskService) -> {
                log.debug("Validating user data");
                Long userId = Long.parseLong(externalTask.getBusinessKey());
                User user = userService.getById(userId);
                externalTaskService.complete(externalTask, Collections.singletonMap("valid", this.isUserDataValid(user)));
            })
            .build();
        externalTaskClient.subscribe(build);
    }

    private boolean isUserDataValid(User user) {
        return !user.getLogin().toLowerCase().equals("admin");
    }
}
