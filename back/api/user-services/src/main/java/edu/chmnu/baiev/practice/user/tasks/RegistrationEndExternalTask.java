package edu.chmnu.baiev.practice.user.tasks;

import edu.chmnu.baiev.practice.core.services.UserService;
import edu.chmnu.baiev.practice.integration.camunda.CamundaExternalTaskClient;
import edu.chmnu.baiev.practice.integration.camunda.ExternalTaskSubscription;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class RegistrationEndExternalTask {
    private final UserService userService;
    private final CamundaExternalTaskClient externalTaskClient;

    public RegistrationEndExternalTask(UserService userService,
                                       CamundaExternalTaskClient externalTaskClient) {
        this.userService = userService;
        this.externalTaskClient = externalTaskClient;
    }

    @PostConstruct
    public void subscribe() {
        ExternalTaskSubscription build = ExternalTaskSubscription.builder()
            .topic("registrationProcessEnd")
            .externalTaskHandler((ExternalTask externalTask, ExternalTaskService externalTaskService) -> {
                String userId = externalTask.getBusinessKey();
                this.userService.setActive(Long.parseLong(userId));
                externalTaskService.complete(externalTask);
            })
            .build();
        externalTaskClient.subscribe(build);
    }
}
