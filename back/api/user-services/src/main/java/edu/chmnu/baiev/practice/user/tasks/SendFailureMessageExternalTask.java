package edu.chmnu.baiev.practice.user.tasks;

import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.services.UserService;
import edu.chmnu.baiev.practice.integration.camunda.CamundaExternalTaskClient;
import edu.chmnu.baiev.practice.integration.camunda.ExternalTaskSubscription;
import edu.chmnu.baiev.practice.user.dto.SendMailDto;
import edu.chmnu.baiev.practice.user.services.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class SendFailureMessageExternalTask {

    private final UserService userService;
    private final CamundaExternalTaskClient externalTaskClient;
    private final EmailService emailService;
    private final MailProperties mailProperties;

    public SendFailureMessageExternalTask(UserService userService,
                                          CamundaExternalTaskClient externalTaskClient,
                                          EmailService emailService,
                                          MailProperties mailProperties) {
        this.userService = userService;
        this.externalTaskClient = externalTaskClient;
        this.emailService = emailService;
        this.mailProperties = mailProperties;
    }

    @PostConstruct
    public void subscribe() {
        ExternalTaskSubscription build = ExternalTaskSubscription.builder()
            .topic("sendBlockedEmail")
            .externalTaskHandler((ExternalTask externalTask, ExternalTaskService externalTaskService) -> {
                log.debug("Sending message, that user is blocked");
                Long userId = Long.parseLong(externalTask.getBusinessKey());
                User user = userService.getById(userId);
                SendMailDto mailDto = SendMailDto.builder()
                    .from(mailProperties.getUsername())
                    .to(user.getEmail())
                    .subject("Practice registration")
                    .content(IOUtils.toInputStream("Your account doesn't meet our terms"))
                    .build();
                emailService.sendMail(mailDto);
                externalTaskService.complete(externalTask);
            })
            .build();
        externalTaskClient.subscribe(build);
    }
}
