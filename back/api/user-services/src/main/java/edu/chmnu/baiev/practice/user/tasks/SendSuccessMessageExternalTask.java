package edu.chmnu.baiev.practice.user.tasks;

import edu.chmnu.baiev.practice.core.db.entities.Token;
import edu.chmnu.baiev.practice.core.db.entities.User;
import edu.chmnu.baiev.practice.core.services.TokenService;
import edu.chmnu.baiev.practice.core.services.UserService;
import edu.chmnu.baiev.practice.integration.camunda.CamundaExternalTaskClient;
import edu.chmnu.baiev.practice.integration.camunda.ExternalTaskSubscription;
import edu.chmnu.baiev.practice.user.dto.SendMailDto;
import edu.chmnu.baiev.practice.user.services.EmailService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class SendSuccessMessageExternalTask {
    private final UserService userService;

    private final CamundaExternalTaskClient externalTaskClient;

    private final EmailService emailService;

    private final MailProperties mailProperties;

    private final TokenService tokenService;

    private final String frontBaseUrl;

    public SendSuccessMessageExternalTask(UserService userService,
                                          CamundaExternalTaskClient externalTaskClient,
                                          EmailService emailService,
                                          MailProperties mailProperties,
                                          TokenService tokenService,
                                          @Value("${front.url}") String frontBaseUrl) {
        this.userService = userService;
        this.externalTaskClient = externalTaskClient;
        this.emailService = emailService;
        this.mailProperties = mailProperties;
        this.tokenService = tokenService;
        this.frontBaseUrl = frontBaseUrl;
    }

    @PostConstruct
    public void subscribe() {
        ExternalTaskSubscription build = ExternalTaskSubscription.builder()
            .topic("sendVerifyEmail")
            .externalTaskHandler((ExternalTask externalTask, ExternalTaskService externalTaskService) -> {
                log.debug("Sending verify message");
                Number tokenId = externalTask.getVariable("tokenId");
                Token token = tokenService.getById(tokenId.longValue());
                Long userId = Long.parseLong(externalTask.getBusinessKey());
                User user = userService.getById(userId);
                String path = UriComponentsBuilder.fromHttpUrl(frontBaseUrl + "/registration/success")
                    .queryParam("token", token.getCode())
                    .build()
                    .toUriString();
                SendMailDto mailDto = SendMailDto.builder()
                    .from(mailProperties.getUsername())
                    .to(user.getEmail())
                    .subject("Practice site registration")
                    .content(IOUtils.toInputStream("You have successfully registered. Go through the link below\n " + path))
                    .build();
                emailService.sendMail(mailDto);
                externalTaskService.complete(externalTask);
            })
            .build();
        externalTaskClient.subscribe(build);
    }
}
