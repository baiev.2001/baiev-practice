DROP TABLE IF EXISTS tokens;
DROP TABLE IF EXISTS users;

CREATE TABLE users
(
    id       INT AUTO_INCREMENT PRIMARY KEY,
    login    VARCHAR(250) UNIQUE NOT NULL,
    password VARCHAR(250)        NOT NULL,
    email    VARCHAR(250) UNIQUE,
    enabled  BOOLEAN DEFAULT false
);

INSERT INTO users (login, password, email, enabled)
VALUES ('test', 'test', null, true);


CREATE TABLE tokens
(
    id      INT AUTO_INCREMENT PRIMARY KEY,
    user_id INT,
    code    char(8),
    CONSTRAINT FK_token_user FOREIGN KEY (user_id) references users (id)
);

