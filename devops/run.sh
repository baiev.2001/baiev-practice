function start() {
    cp ../back/api/user-services/target/user-services*.jar ./dockerfiles/api/user-service/user-services.jar;
    cp ../back/camunda/target/camunda*.jar ./dockerfiles/camunda/camunda.jar;
    docker-compose up -d --build    ;
}
function stop() {
    docker-compose down
}

if [[ -z "$FRONT_END_ROOT" ]];
then
    CURRENT_DIR="$(dirname $(pwd))";
    export FRONT_END_ROOT="$CURRENT_DIR/front";
fi

if [[ -z "$MAIL_PROTOCOL" ]];
then
    export MAIL_PROTOCOL="smtp";
fi

if [[ -z "$MAIL_HOST" ]];
then
    export MAIL_HOST="smtp.gmail.com";
fi

if [[ -z "$MAIL_PORT" ]];
then
    export MAIL_PORT="587";
fi

if [[ -z "$MAIL_USER" ]];
then
    export MAIL_USER="";
fi

if [[ -z "$MAIL_PASSWORD" ]];
then
    export MAIL_PASSWORD="";
fi

"$@"
