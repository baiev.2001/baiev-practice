import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { AuthActivateService } from './core/guard/auth-activate.service';
import { LogInActivateService } from './core/guard/log-in-activate.service';


const routes: Routes = [
  {
    path: 'home',
    component: HomePageComponent,
    canActivate: [AuthActivateService]
  },
  {
    path: 'registration',
    loadChildren: './registration/registration.module#RegistrationModule',
    canActivateChild: [LogInActivateService]
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
    canActivate: [LogInActivateService]
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
