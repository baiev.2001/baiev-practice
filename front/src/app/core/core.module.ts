import { NgModule } from '@angular/core';
import { CORE_SERVICES } from './services';
import { HttpClientModule } from '@angular/common/http';
import { GUARD_SERVICES } from './guard';
import { INTERCEPTORS } from './interceptors';

@NgModule({
  imports: [HttpClientModule],
  declarations: [],
  providers: [CORE_SERVICES, GUARD_SERVICES, INTERCEPTORS],
  exports: []
})
export class CoreModule {

}
