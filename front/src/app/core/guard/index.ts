import { AuthActivateService } from './auth-activate.service';
import { LogInActivateService } from './log-in-activate.service';

export const GUARD_SERVICES = [
  AuthActivateService,
  LogInActivateService
];
