import { AuthTokenInterceptorService } from './auth-token-interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

export const INTERCEPTORS = [
  {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthTokenInterceptorService,
    multi: true
  }
];
