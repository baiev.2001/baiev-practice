import { JwtHelperService } from '@auth0/angular-jwt';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthStorageService {
  private storage = localStorage;
  private jwtHelperService = new JwtHelperService();

  public store(token: string) {
    this.storage.setItem('token', token);
  }

  public get(): string {
    return this.storage.getItem('token');
  }

  public remove() {
    this.storage.removeItem('token');
  }

  public isTokenExpired() {
    return this.jwtHelperService.isTokenExpired(this.get());
  }

  public getLogin(): any {
    const token = this.get();
    return token && this.jwtHelperService.decodeToken(token).sub;
  }
}
