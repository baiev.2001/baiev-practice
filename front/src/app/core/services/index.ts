import { UserService } from './user.service';
import { AuthStorageService } from './auth-storage.service';

export const CORE_SERVICES = [
  UserService,
  AuthStorageService
];
