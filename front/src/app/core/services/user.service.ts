import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserModel } from '../models/user.model';
import { LoginModel } from '../models/login.model';
import { map } from 'rxjs/operators';
import { AuthStorageService } from './auth-storage.service';
import { AuthModel } from '../models/auth.model';
import { UserInfoModel } from '../models/user-info.model';
import { Router } from '@angular/router';

@Injectable()
export class UserService {
  constructor(private httpClient: HttpClient,
              private authStorage: AuthStorageService,
              private router: Router) {
  }

  register(userInfo: UserModel) {
    return this.httpClient.post('api/user/register', userInfo);
  }

  login(loginModel: LoginModel) {
    return this.httpClient.post<AuthModel>('api/user/auth', loginModel)
      .pipe(map(response => {
        if (response && response.token) {
          this.authStorage.store(response.token);
        }
        return response.token;
      }));
  }

  getInfo() {
    return this.httpClient.get<UserInfoModel>('api/user/info');
  }

  isLoggedIn() {
    const token = this.authStorage.get();
    return token && token !== '' && !this.authStorage.isTokenExpired();
  }

  getToken() {
    return this.authStorage.get();
  }

  logout() {
    this.authStorage.remove();
    this.router.navigate(['login']);
  }

  verify(token: string) {
    return this.httpClient.post('api/user/register/confirm', token);
  }
}
