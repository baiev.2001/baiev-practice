import { Component, OnInit } from '@angular/core';
import { UserService } from '../core/services/user.service';
import { UserInfoModel } from '../core/models/user-info.model';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  private userInfo: UserInfoModel;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getInfo().subscribe(
      next => {
        this.userInfo = next;
      }
    );
  }

  logout() {
    this.userService.logout();
  }
}
