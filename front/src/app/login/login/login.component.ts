import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginModel } from '../../core/models/login.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private loginForm: FormGroup;

  private errorMessage: string;


  constructor(private service: UserService,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.loginForm = formBuilder.group({
      login: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit() {
  }

  login(loginModel: LoginModel) {
    this.service.login(loginModel)
      .subscribe(response => {
          this.router.navigate(['home']);
        },
        error => {
          this.errorMessage = 'Your account still does not exists.Wait while it will activate, or register a new one';
        });
  }
}
