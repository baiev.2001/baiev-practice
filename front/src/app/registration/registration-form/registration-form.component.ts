import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserModel } from '../../core/models/user.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.css']
})
export class RegistrationFormComponent implements OnInit {
  private registerUserForm: FormGroup;
  private errorMessage: string;

  constructor(private userService: UserService,
              private router: Router,
              private formBuilder: FormBuilder) {
    this.registerUserForm = formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      login: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }


  register(userInfo: UserModel) {
    this.userService
      .register(userInfo)
      .subscribe((response) => {
        console.log(response);
        this.router.navigate(['registration/verify']);
      }, error => {
        this.errorMessage = 'Something wrong with your data. Try another one';
      });
  }
}
