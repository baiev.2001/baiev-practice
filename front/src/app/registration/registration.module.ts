import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { VerifyComponent } from './verify/verify.component';
import { SuccessComponent } from './success/success.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationRoutingModule } from './registration-routing.module';


@NgModule({
  declarations: [RegistrationFormComponent, VerifyComponent, SuccessComponent],
  imports: [
    FormsModule,
    RegistrationRoutingModule,
    ReactiveFormsModule,
    CommonModule
  ]
})
export class RegistrationModule {
}
