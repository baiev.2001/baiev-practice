import { Component, OnInit } from '@angular/core';
import { UserService } from '../../core/services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {
  private queryParams;

  constructor(private userService: UserService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParamMap.subscribe((value => this.queryParams = value));
    const token = this.queryParams.get('token');
    this.userService.verify(token).subscribe(
      next => {
        this.router.navigate(['/login']);
      },
      error => {
        console.warn('Cannot activate user');
        console.error(error);
      }
    );
  }
}
